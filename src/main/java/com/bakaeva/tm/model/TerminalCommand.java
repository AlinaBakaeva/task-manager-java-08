package com.bakaeva.tm.model;

import com.bakaeva.tm.constant.TerminalConst;

public class TerminalCommand {
    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.CMD_HELP, TerminalConst.ARG_HELP, "Display terminal commands."
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.CMD_ABOUT, TerminalConst.ARG_ABOUT, "Show developer info."
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.CMD_VERSION, TerminalConst.ARG_VERSION, "Show version info."
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.CMD_INFO, TerminalConst.ARG_INFO, "Display information about system."
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.CMD_EXIT, null, "Close application."
    );

    private String name = "";

    private String arg = "";

    private String description = "";

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }
}


